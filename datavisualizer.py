import csv
import gudhi
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 


data = pd.read_csv("dream_market_cocaine_listings.csv")

#grab headers as seperate arrays
#didnt grab the seperate to and from shipping

#INT ARRAYS
successful_transactions = data['successful_transactions']
rating = data['rating']
grams = data['grams']
quality = data['quality']
btc_price = data['btc_price']
cost_per_gram = data['cost_per_gram']
cost_per_gram_pure = data['cost_per_gram_pure']
escrow = data['escrow']

#STRING ARRAYS
product_link = data['product_link']
vendor_link = data['vendor_link']
vendor_name = data['vendor_name']
ships_from = data['ships_from']
ships_to = data['ships_to']
product_title = data['product_title']
ships_from_to = data['ships_from_to']

data0 = data[data['ships_from_NL'] == True]
data1 = data[data['ships_from_US'] == True]

fig = plt.figure(figsize= (10,10))
ax = fig.add_subplot(111)
ax.scatter(btc_price,cost_per_gram, c = 'b', marker = ".", label = 'Ships to US')
#ax.scatter(data1['quality'],data1['cost_per_gram'], c = 'r', marker = ".",label = 'Ships to Other')
ax.set_xlabel('btc_price')
ax.set_ylabel('cost per gram')
plt.title('the cost per gram x quality depending on shipment from US')
plt.legend()
plt.show()

#print(data.head()) #displays first 5 entries

fig = plt.figure(figsize= (10,10))
ax = fig.add_subplot(111)
ax.scatter(quality,cost_per_gram, c = 'b', marker = ".",label = 'cost per gram')
ax.scatter(quality,cost_per_gram_pure, c = 'r', marker = ".",label = 'cost per gram pure')
ax.set_xlabel('quality')
ax.set_ylabel('cost per gram')
plt.title('quality x cost per gram')
plt.legend()
plt.show()

fig = plt.figure(figsize= (10,10))
ax = fig.add_subplot(111)
ax.scatter(quality,btc_price, c = 'b', marker = ".")
ax.set_xlabel('quality')
ax.set_ylabel('btc price')
plt.title('quality x btc price')
plt.show()

fig = plt.figure(figsize= (15,15))
ax = Axes3D(fig)

ax.scatter(quality, cost_per_gram, rating, c='b', marker=".",label = 'cost per gram')
ax.scatter(quality, cost_per_gram_pure, rating, c='r', marker=".",label = 'cost per gram pure')

ax.set_xlabel('quality')
ax.set_ylabel('cost per gram')
ax.set_zlabel('rating')

plt.title('quality x cost per gram x rating')
plt.legend()
plt.show()

points = []

for i in range(len(btc_price)):
    points.append((grams[i],cost_per_gram[i]))
    
rips_complex = gudhi.RipsComplex(points, max_edge_length=.00001)

simplex_tree = rips_complex.create_simplex_tree(max_dimension=3)
result_str = 'Rips complex is of dimension ' + repr(simplex_tree.dimension()) + ' - ' + \
    repr(simplex_tree.num_simplices()) + ' simplices - ' + \
    repr(simplex_tree.num_vertices()) + ' vertices.'
print(result_str)
'''
fmt = '%s -> %.8f'
for filtered_value in simplex_tree.get_filtration():
    print(fmt % tuple(filtered_value))
    print(filtered_value)
'''
count2 = 0
count3 = 0
bignodes =[]
bignodes4=[]
for nodes, value in simplex_tree.get_filtration():
    #print(nodes)
    if len(nodes) == 3:
        bignodes.append(nodes)
        count2+=1
    if len(nodes) == 4:
        bignodes4.append(nodes)
        count3+=1

print(f'Two Dimensional: {count2}')
print(f'Three Dimensional: {count3}')
#print(bignodes)
'''
fig = plt.figure(figsize=(10,10))
for num in bignodes:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    for i in range(len(points)):
        pts.append([(grams[points[0]],cost_per_gram[points[0]]),(grams[points[1]],cost_per_gram[points[1]])])
        pts.append([(grams[points[1]],cost_per_gram[points[1]]),(grams[points[2]],cost_per_gram[points[2]])])
        pts.append([(grams[points[0]],cost_per_gram[points[0]]),(grams[points[2]],cost_per_gram[points[2]])])
    polygon = plt.Polygon(pts, alpha =0.05)
    plt.scatter([cost_per_gram[points[0]],cost_per_gram[points[1]],cost_per_gram[points[0]]],[cost_per_gram[points[1]],cost_per_gram[points[2]],cost_per_gram[points[2]]], color = 'r')    
    plt.gca().add_patch(polygon)

plt.axis('scaled')
plt.show()
'''
fig = plt.figure(figsize=(10,10))
for num in bignodes:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    for i in range(len(points)):
        pts.append([points[0],points[1]])
        pts.append([points[1],points[2]])
        pts.append([points[0],points[2]])
    polygon = plt.Polygon(pts, alpha =0.05)
    plt.scatter([points[0],points[1],points[0]],[points[1],points[2],points[2]], color = 'r')    
    plt.gca().add_patch(polygon)

plt.axis('scaled')
plt.show()

fig = plt.figure(figsize=(10,10))

for num in bignodes4:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    for i in range(len(points)):
        pts.append([points[0],points[1]])
        pts.append([points[1],points[2]])
        pts.append([points[1],points[3]])
        pts.append([points[2],points[3]])
        pts.append([points[0],points[3]])
        pts.append([points[0],points[2]])
    polygon = plt.Polygon(pts, color ='b',alpha =.05)
    plt.gca().add_patch(polygon)
    plt.scatter([points[0],points[1],points[1],points[2],points[0],points[0]],[points[1],points[2],points[3],points[3],points[3],points[2]], color = 'r')

#plt.scatter(grams,cost_per_gram, c = 'r', marker = ".", label = 'Ships to US')
plt.axis('scaled')
plt.show()

fig = plt.figure(figsize=(10,10))
plt.subplot(311)
for num in bignodes:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    plt.scatter([points[0],points[1],points[0]],[points[1],points[2],points[2]], color = 'r')

plt.subplot(312)
for num in bignodes4:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    plt.scatter([points[0],points[1],points[1],points[2],points[0],points[0]],[points[1],points[2],points[3],points[3],points[3],points[2]], color = 'b')

plt.subplot(313)
for num in bignodes:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    plt.scatter([points[0],points[1],points[0]],[points[1],points[2],points[2]], color = 'r')

for num in bignodes4:
    #print(num)
    points = []
    pts = []
    for val in num:
        points.append(val)
    plt.scatter([points[0],points[1],points[1],points[2],points[0],points[0]],[points[1],points[2],points[3],points[3],points[3],points[2]], color = 'b')

plt.axis('scaled')
plt.show()
''' 
fig = plt.figure()
points = [[2, 1], [8, 1], [8, 4]]
polygon = plt.Polygon(points)
plt.gca().add_patch(polygon)
plt.axis('scaled')
plt.show()
'''